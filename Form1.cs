﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using modbus485rtu;
using HslCommunication;
using HslControls;

namespace 模拟量485读取demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public M_485Rtu com;
        COMconfig com_conf;
        Queue<double>[] dataQueue = new Queue<double>[8];//保留了时间顺序的队列，一共八个通道，八个队列，每个队列长度为10，代表了每秒10个数据，这里可以和电机的角度采集进行对应，一起打上时间戳。
        System.Timers.Timer monitorTimer;
        private void Form1_Load(object sender, EventArgs e)
        {
            for(int i = 0; i < 8; i++)
            {
                dataQueue[i] = new Queue<double>();
            }

            com_conf.botelv = "19200";
            com_conf.zhanhao = "1";//站号
            com_conf.shujuwei = "8";
            com_conf.tingzhiwei = "1";
            com_conf.dataFromZero = true;
            com_conf.stringReverse = false;
            com_conf.COM_Name = "COM3";
            com_conf.checkInfo = 2;
            com_conf.dataFrame = 2;
            com = new M_485Rtu(com_conf);
            com.connect();

            monitorTimer = new System.Timers.Timer(100);
            monitorTimer.Elapsed += (o, a) =>
            {
                MonitorActive();
            };//到达时间的时候执行事件； 
            monitorTimer.AutoReset = true;//设置是执行一次（false）还是一直执行(true)；
            monitorTimer.Enabled = true;//是否执行System.Timers.Timer.Elapsed事件；

        }

        private void MonitorActive()
        {             
            //获取报文中的数据部分
            byte[] res = HslCommunication.BasicFramework.SoftBasic.HexStringToBytes(com.ReadFrame(textBox1.Text));
            for (int i = 0; i < res[2]; i++)
            {
                data[i] = res[i + 3];
            }

            // t_data[0] = com.bytes2ushort(data[0], data[1]);
            //将报文数据部分组合成各个通道的short数据
            for (int i = 0; i < res[2]; i += 2)
            {
                t_data[i / 2] = com.bytes2ushort(data[i], data[i + 1]);
            }
            DateTime t = DateTime.Now;            
            //Console.WriteLine(t.ToString("yyyy-MM-dd hh:mm:ss:fff"));
            //res[2]==16
            for (int i = 0; i < res[2]; i += 2)
            {
                int pos = i / 2;
              //  Console.WriteLine("Data "+pos+" :"+data[pos]);

                if (dataQueue[pos].Count < 10)//当数据长度小于10 那么一直往队列里面加数据
                {
                    dataQueue[pos].Enqueue(data[pos]);
                }
                else//当数据到了10 ，先出队一个数据再添加一个数据，保证数据的时间顺序是有一个先来后到的顺序
                {
                    dataQueue[pos].Dequeue();//remove the first data
                    dataQueue[pos].Enqueue(data[pos]);
                    dataQueue[pos].Last();//返回队列中最后一个元素用于实时曲线的显示。
                }
            }
            //遍历获取每个通道1s内的十个数据
            //foreach(var item in dataQueue[1])
            //    Console.WriteLine("Data " + item);
            //Console.WriteLine("\n=================================");
        }
        byte[] data = new byte[16];
        ushort[] t_data = new ushort[8];
        private void Button1_Click(object sender, EventArgs e)
        {   //读取八个数据
            //Console.WriteLine(HslCommunication.Serial.SoftCRC16.CRC16(HslCommunication.BasicFramework.SoftBasic.HexStringToBytes(textBox1.Text)).ToString());
             textBox2.Text=com.ReadFrame(textBox1.Text);
            //textBox2.Text = HslCommunication.BasicFramework.SoftBasic.HexStringToBytes(textBox1.Text)[4].ToString();
            //获取报文中的数据部分
            byte[] res = HslCommunication.BasicFramework.SoftBasic.HexStringToBytes(textBox1.Text);
            for (int i = 0; i < res[2]; i++)
            {
                data[i] = res[i + 3];
            }

            // t_data[0] = com.bytes2ushort(data[0], data[1]);
            //将报文数据部分组合成各个通道的short数据
            for (int i = 0; i < res[2]; i += 2)
            {
                t_data[i/2] = com.bytes2ushort(data[i], data[i+1]);
            }

            //量程转换标准   最小量程-最大量程    对应   0-65535
            //

        }
    }
}
